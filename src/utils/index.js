// 存放项目公用工具或函数
const tools = {
  // 产生随机数，len：长度
  getUUID: (len) => {
    len = len || 6
    len = parseInt(len, 10)
    len = isNaN(len) ? 6 : len
    const seed = '0123456789abcdefghijklmnopqrstubwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ'
    const seedLen = seed.length - 1
    let uuid = ''
    while (len--) {
      uuid += seed[Math.round(Math.random() * seedLen)]
    }
    return uuid
  },
  // 深拷贝
  deepCopy: (data) => {
    if (!data) {
      return data
    }
    let sourceCopy = data instanceof Array ? [] : {}
    for (let item in data) {
      sourceCopy[item] = typeof data[item] === 'object' ? this.deepcopy(data[item]) : data[item]
    }
    return sourceCopy
  },
  // 清除当前所有的缓存
  clearStorage: (item) => {
    const _item = item || ''
    // 如果不传入参数值，默认删除所有缓存
    if (_item === '') {
      sessionStorage.clear()
      localStorage.clear()
    } else {
      sessionStorage.removeItem(_item)
      localStorage.removeItem(_item)
    }
  }
}

export default tools
