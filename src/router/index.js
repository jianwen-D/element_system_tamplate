import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'// progress bar style
import store from '@/store'
import tools from './../utils'

Vue.use(Router)
// 路由目录
const router = new Router({
  scrollBehavior: () => ({ y: 0 }), // 页面回到顶部
  routes: [
    {
      path: '/login',
      name: 'login',
      component: (resolve) => require(['@/views/login'], resolve)
    },
    {
      path: '/',
      name: 'layout',
      component: (resolve) => require(['@/views/layout'], resolve),
      children: [
        {
          path: 'test1',
          name: '基本表格测试',
          title: '基本表格测试',
          group: '1',
          groupTitle: '页面测试',
          single: false,
          component: (resolve) => require(['@/views/test1'], resolve)
        },
        {
          path: 'test3',
          name: '其他页面测试',
          title: '其他页面测试',
          group: '2',
          groupTitle: '2',
          single: true,
          component: (resolve) => require(['@/views/test2'], resolve)
        }
      ]
    },
    {
      path: '/404',
      name: '404',
      component: (resolve) => require(['@/views/errorPage/404'], resolve)
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})

// 路由拦截器
// 跳转路由前，需要做什么，to：目的路由，from：当前路由，next：跳转路由
router.beforeEach((to, from, next) => {
  // 动态添加导航历史标签页
  store.dispatch('saveNavTabList', to)
  // 开启加载条
  NProgress.start()
  // 判断是否进入默认地址
  if (to.path === '/login') {
    // 清除浏览器缓存
    tools.clearStorage()
  }
  // 判断是否登陆
  if (!sessionStorage.getItem('token') && to.path !== '/login') {
    next({ path: '/login' })
    NProgress.done()
  } else {
    next()
  }
})
// 后置的路由拦截器
router.afterEach(transition => {
  NProgress.done()
})

export default router
