// 开发环境
const development = {
  baseUrl: 'http://localhost:3000',
  whiteList: []
}

// 生产环境
const product = {
  baseUrl: 'http://localhost:3000',
  whiteList: ['']
}

export default process.env.NODE_ENV === 'development' ? development : product
